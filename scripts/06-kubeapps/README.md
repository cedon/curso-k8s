# Tutorial Kubeapps

    - Tutorial de instalação e configuração do kubeapps

## Install Kubeapps

    - helm repo add bitnami https://charts.bitnami.com/bitnami
    - helm install --name kubeapps --namespace kubeapps bitnami/kubeapps

## Create API token

    - kubectl create serviceaccount kubeapps-operator
    - kubectl create clusterrolebinding kubeapps-operator --clusterrole=cluster-admin --serviceaccount=default:kubeapps-operator

## Get the token

    - kubectl get secret $(kubectl get serviceaccount kubeapps-operator -o jsonpath='{.secrets[].name}') -o jsonpath='{.data.token}' | base64 --decode && echo

## Starts the Kubeapps Dashboard

    - No Master
    - export POD_NAME=$(kubectl get pods -n kubeapps -l "app=kubeapps,release=kubeapps" -o jsonpath="{.items[0].metadata.name}")
    - echo "Visit http://127.0.0.1:8080 in your browser to access the Kubeapps Dashboard"
    - kubectl port-forward -n kubeapps $POD_NAME 8080:8080

    - No cliente
    - ssh aluno@200.129.137.XXX -L 8080:127.0.0.1:8080 -N
    - acessar no navegador http://127.0.0.1:8080 

