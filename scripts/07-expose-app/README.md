# Tuto Expor um Aplicativo através um Serviço em uma porta padrão(80/443)

## About

    - Material Curso de Kubernetes
    
## Usage

    - curl-pod.yaml: arquivo de deploy de aplicação para testar aplicações internamente
    - nginx-deploy.yaml: faz o deploy da imagem do nignx no cluster.
    - nginx-service.yaml: configura o serviço que vai expor a aplicação my-nginx.
    - nginx-secret.yaml: faz deploy do secret de um serviço no cluster. 
    - nginx-secure-app.yaml: faz deploy de uma versão com https do applicativo my-nginx.
    
## Fazer o Deploy do nginx

    - kubectl apply -f nginx-deploy.yaml
    - kubectl get pods -l run=my-nginx -o wide
    
## Verificar IPs

    - kubectl get pods -l run=my-nginx -o yaml | grep podIP
    
## Criar o Serviço

    - kubectl apply -f nginx-service.yaml
    
## Monitorar serviços e endpoints

    - kubectl get svc my-nginx
    - kubectl describe svc my-nginx
    - kubectl get ep my-nginx
    
## Checando variáveis de ambiente dos Pods

    - kubectl get pods
    - kubectl exec my-nginx-[nome_do_pod] -- printenv | grep SERVICE
    
## Criando Secret para o serviço

    - (create a public private key pair)
    - openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout \
    /tmp/nginx.key -out /tmp/nginx.crt -subj "/CN=my-nginx/O=my-nginx"
    - (convert the keys to base64 encoding)
    - cat /tmp/nginx.crt | base64
    - cat /tmp/nginx.key | base64
    - kubectl apply -f nginx-secret.yaml
    - kubectl get secrets
    
## Fazer Deploy da versão do app com segurança

    - kubectl delete deployments,svc my-nginx; kubectl create -f ./nginx-secure-app.yaml

## Testar acesso a aplicação pelo IP de um Pod

    - kubectl get pods -l run=my-nginx -o yaml | grep podIP
    podIP: 10.244.3.5
    node $ curl -k https://10.244.3.5
    ...
    <h1>Welcome to nginx!</h1>

## Testar de dentro da rede dos pods com o comando CURL

    - kubectl apply -f ./curl-pod.yaml
    - kubectl get pods -l app=curlpod
    - kubectl exec curl-deployment-1515033274-1410r -- curl https://my-nginx --cacert /etc/nginx/ssl/nginx.crt

## Expor o serviço 

    - kubectl get svc my-nginx -o yaml | grep nodePort -C 5
    - kubectl get nodes -o yaml | grep ExternalIP -C 1
    - curl https://<EXTERNAL-IP>:<NODE-PORT> -k
    (modificar o tipo de NodePort para LoadBalancer)
    - kubectl edit svc my-nginx
    - kubectl get svc my-nginx
    - curl https://<EXTERNAL-IP> -k
    
    



