# Tuto HTTP SERVER

## About

    - Material Curso de Kubernetes
    
## Usage

    - deployment.yaml: faz o deploy da imagem katacoda/docker-http-server:latest na aplicação webapp1 no cluster.
    - service.yaml: configura o serviço que vai expor a aplicação webapp1.


## Tuto
    
### Deploy do Ambiente
    - kubectl create -f deployment.yaml
    - kubectl get deployment
    - kubectl describe deployment webapp1

### Deploy dos serviços
    - kubectl create -f service.yaml
    - kubectl get svc
    - kubectl describe svc webapp1-svc

### Escalar pods mudando o parametro Replicas para 4    
    - kubectl apply -f deployment.yaml
    - kubectl get deployment
    - kubectl get pods

