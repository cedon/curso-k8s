#!/bin/bash

curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -

touch /etc/apt/sources.list.d/kubernetes.list

echo 'deb http://apt.kubernetes.io/ kubernetes-xenial main' > /etc/apt/sources.list.d/kubernetes.list

apt -y -qq update && apt -y -qq upgrade

apt install -y kubeadm=1.15.4-00 kubelet=1.15.4-00 kubectl=1.15.4-00
