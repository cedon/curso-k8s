# Tuto Cluster Kubernetes

## About

    - Tutorial de instalação e configuração de cluster kubernetes +  Deploy de app do Nginx + Dashboard
    
## Usage

    - docker.sh: instalar o docker nos nodes
    - kubernetes.sh: instalar kubeadm kubelet e kubectl nos nodes
    - weave-kube: script que configura rede interna do cluster
    - clusterrolingbinding.sh: configuração do usuário do dashboard
    - kube.sh: configurar cliente do cluster
    - nginx-deployment.yaml: YAML de deploy de um servidore web nginx
    - nginx-service.yaml: YAML de deploy do serviço para o nginx

## Requisitos

    - Duas VMs
    - kubeadm, kubelet e kubectl

## Tuto

    - (executar estas configurações em todos os nós)
    - Instalar o Docker
    ---- apt update -y -qq && apt upgrade -y -qq
    ---- curl -fsSL https://get.docker.com | sh;
    ---- (ou usar o script) sh docker.sh

    - rebootar a VM
    - Instalar o Kubernetes
    ---- sh kubernetes.sh

    - Configurar /etc/hosts em todas as VMs
    ----(acrescentar o ip e nome dos nodes do cluster)
    ----(ex.:
    ----200.129.137.XXX   nome-master
    ----200.129.137.XXX   nome-worker)

    - Modificar /etc/hostname de cada VM (deve ficar igual ao nome do node que está no /etc/hosts)
    ---- no Master: nome-master
    ---- no Worker: nome-worker

    - Configurar Firewall para Laboratório
    ---- sudo cp firewall /etc/init.d/firewall ## (copia arquivo firewall para /etc/init.d/firewall)
    ---- sudo cp firewall.service /usr/lib/systemd/system/firewall.service  ## copiar arquivo firewall.service para /usr/lib/systemd/system/firewall.service
    ---- sudo chmod 755 /etc/init.d/firewall && chmod 644 /usr/lib/systemd/system/firewall.service
    ---- sudo systemctl enable firewall.service
    ---- systemctl start firewall.service

    
## Inicializar o Cluster

    - (executar estas configurações no Master)
    - kubeadm init --pod-network-cidr=10.244.0.0/16 --service-cidr=10.244.10.0/12 --ignore-preflight-errors=cri \
    --apiserver-advertise-address=ACRESCENTAR_IP_DO_MASTER --kubernetes-version "1.15.4"
    - (ou) kubeadm init --token=102952.1a7dd4cc8d1f4cc5 --kubernetes-version $(kubeadm version -o short)

## Configurar cliente

    - (no lab, executar estes comandos no Master)
    - sudo cp /etc/kubernetes/admin.conf $HOME/
    - sudo chown $(id -u):$(id -g) $HOME/admin.conf
    - export KUBECONFIG=$HOME/admin.conf
    - (ou) sh kube.sh

## Acrescentar Worker Node no Cluster

    - kubeadm token list (executar no Master)
    - (Coletar Hash)
    (abaixo executar no Worker Node que será ingressado no cluster)
    - kubeadm join --discovery-token-unsafe-skip-ca-verification --token=HASH_DO_TOKEN_COLETADO 172.17.0.16:6443
    - kubectl get nodes (executar no Master)
    
## Configurar rede interna do cluster

### Flannel

    - (executar no Master)
    - kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml
    
