# Tuto Stateful APPS - criando volumes (PV e PVC)

## About

    - Material Curso de Kubernetes
    
## Usage

    - nfs-0001.yaml: arquivo YAML de criação do pv da partição /export/nfs-0001
    - nfs-0002.yaml: arquivo YAML de criação do pv da partição /export/nfs-0002
    - pvc-mysql.yaml: arquivo YAML de criação do pvc para o pod mysql
    - pvc-www.yaml: arquivo YAML de criação do pvc para o nginx
    - pod-mysql.yaml: arquivo YAML para deploy de um pod mysql que 
    faz referencia aos volumes criados anteriormente
    - pod-http.yaml: arquivo YAML para deploy de um pod nginx que 
    faz referencia aos volumes criados anteriormente

## Instalar Servidor NFS

    - docker run -d --net=ENDERECO_IP_VM \ --privileged --name nfs-server \ katacoda/contained-nfs-server:centos7 \
    /exports/data-0001 /exports/data-0002
    
## Fazer o Deploy dos PVs

    - kubectl create -f nfs-0001.yaml
    - kubectl create -f nfs-0002.yaml
        
## Fazer o Deploy dos PVCs

    - kubectl create -f pvc-http.yaml
    - kubectl create -f pvc-mysql.yaml
        
## Fazer o Deploy dos pods

    - kubectl create -f pod-www.yaml
    - kubectl create -f pod-mysql.yaml
        
## Criar uma página index.html no volume do servidor web no docker do Servidor NFS

    -docker exec -it nfs-server bash -c \ 
    "echo 'Hello World' > /exports/data-0001/index.html"

## Verificar IP do pod e carregar a página por ele

    -ip=$(kubectl get pod www -o yaml |grep podIP | \
    awk '{split($0,a,":"); print a[2]}'); echo $ip
    - curl $ip

## Atualizar Dados

    - docker exec -it nfs-server bash -c "echo 'Hello NFS World' \
    > /exports/data-0001/index.html"
    - curl $ip

## Deletar pod www e levantar um novo carregando o mesmo volume

    - kubectl delete pod www
    - kubectl create -f pod-www2.yaml

## Testar modificações

    - ip=$(kubectl get pod www2 -o yaml |grep podIP | \ 
    awk '{split($0,a,":"); print a[2]}'); curl $ip