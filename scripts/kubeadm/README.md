# Tuto Kubeadm

## About

    - Material Curso de Kubernetes
    
## Usage

    - secret.yml: arquivo com configurações de senhas para o MySql e Wordpress
    - docker.sh: instalar o docker nos nodes
    - kubernetessh: instalar kubeadm kubelet e kubectl nos nodes
    - weave-kube: script que configura rede interna do cluster
    - clusterrolingbinding.sh: configuração do usuário do dashboard
    - kube.sh: configurar cliente do cluster
    - nginx-deployment.yaml: YAML de deploy de um servidore web nginx
    - nginx-service.yaml: YAML de deploy do serviço para o nginx

## Requisitos

    - Duas VMs
    - kubeadm

## Tuto

    - Configurar /etc/hosts
    - curl -fsSL https://get.docker.com | sh;
    - (ou) sh docker.sh
    - rebootar a VM
    - sh kubernetes.sh
    
## Inicializar o Cluster

    - kubeadm init --pod-network-cidr=10.244.10.0/16 --service-cidr=10.244.10.0/12 --ignore-preflight-errors=cri \
    --apiserver-advertise-address=10.15.10.43 --kubernetes-version "1.14.0"
    - (ou) kubeadm init --token=102952.1a7dd4cc8d1f4cc5 --kubernetes-version $(kubeadm version -o short)

## Configurar cliente

    - sudo cp /etc/kubernetes/admin.conf $HOME/
    - sudo chown $(id -u):$(id -g) $HOME/admin.conf
    - export KUBECONFIG=$HOME/admin.conf
    - (ou) sh kube.sh
    - kubeadm token list
    - kubeadm join --discovery-token-unsafe-skip-ca-verification --token=102952.1a7dd4cc8d1f4cc5 172.17.0.16:6443
    - kubectl get nodes
    
## Configurar rede interna do cluster

### Flannel

    - kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml
    
### Weave-kube

    - kubectl apply -f weave-kube
    - kubectl get pod -n kube-system
    
## Deploy de imagem servidor http

    - kubectl create deployment http --image=katacoda/docker-http-server:latest
    
## Deploy do nginx

    - kubectl create -f nginx-deployment.yaml
    - kubectl get pods
    - kubectl describe deployment nginx-deployment
    - kubectl get pods
    - kubectl describe pods nginx-deployment-9dd464697-88lzw (exemplo de nome)
    - sudo nano nginx-service.yaml
    - kubectl create -f nginx-service.yaml
    - kubectl describe service nginx-service
    
## Listar pods e serviços

    - kubectl get pods
    - kubectl get service
    - kubectl get svc

## No worker node

    - docker ps | grep docker-http-server

## No Master

    - kubectl apply -f dashboard.yaml
    - kubectl get pods -n kube-system
    - sh clusterrolingbinding.sh
    - kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep admin-user | awk '{print $1}')
    - Copiar token que foi impresso na tela

## Referencias

    - https://github.com/kubernetes/dashboard/wiki/Creating-sample-user
    - https://www.howtoforge.com/tutorial/how-to-install-kubernetes-on-ubuntu/
    - https://kubernetes.io/docs/setup/independent/create-cluster-kubeadm/

