#!/bin/bash

apt update -y -qq && apt upgrade -y -qq

apt install  apt-transport-https  ca-certificates  curl  gnupg2  software-properties-common -y -qq

curl -fsSL  https://get.docker.com | sh

systemctl start docker

systemctl enable docker

swapoff -a
