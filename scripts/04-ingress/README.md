# Tutorial Ingress+Proxy+ClusterIP Deploy

## Config HA PROXY

    - logar como root na VM do HA PROXY
    - apt install haproxy
    - copiar o conteúdo do arquivo haproxy.cfg para /etc/haproxy/haproxy.cfg
    - acrescenar os IPs do(s) worker(s) 
    - $ systemctl status haproxy
    - $ systemctl enable haproxy
    - $ systemctl start haproxy

## Create Ingress Controller

    - git clone https://github.com/nginxinc/kubernetes-ingress.git
    - kubectl apply -f kubernetes-ingress/deployments/common/ns-and-sa.yaml
    - kubectl apply -f kubernetes-ingress/deployments/common/default-server-secret.yaml
    - kubectl apply -f kubernetes-ingress/deployments/common/nginx-config.yaml
    - kubectl apply -f kubernetes-ingress/deployments/rbac/rbac.yaml
    - kubectl apply -f kubernetes-ingress/deployments/daemon-set/nginx-ingress.yaml

## Check de installation

    - kubectl get all -n nginx-ingress

## Deploy the Apps
    
    - kubectl create -f deployment.yaml

## Deploy the Ingress Resource

    - kubectl create -f ingress-rules.yaml

## Edit /etc/hosts

    - acrescentar o conteúdo do arquivo etc.hosts ao final do arquivo de hosts
    - substituir os ips pelo IP do HA PROXY