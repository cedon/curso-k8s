# Tutorial Helm

## About

    - Material curso kubernetes
    
## Usage
    - rbac-config.yaml: yaml de configuração do usuário tiller para deploy via helm

## Helm components

    - Helm Client: A chart manager.
    - tiller Server: Helm server manages releases (installations) of your charts.
    - Charts: packages of pre-configured Kubernetes resources.
    - Release: a collection of Kubernetes resources deployed to the cluster using Helm.

## Basic Commands

    - helm init: instala o tiller
    - helm list: lista as releases em produção
    - helm list -a: lista todas as releases
    - helm repo update: atualiza os repositórios
    - helm search: lista os pacotes disponíveis no repositório
    - helm install stable/mysql: instala uma imagem no cluster
    - helm install --name mysql-release stable/mysql: instala imagem e armazena informações da release
    - helm inspect stable/mysql: inspeciona a imagem no repositório
    - helm status $=helm_name: descreve o status da release
    - helm rollback $=helm_name NUMERO_ROLLBACKS: retorna a versões anteriores da release
    - helm delete $=helm_name: remove a release
    - helm delete --purge $=helm_name: lista a release que foi removida
    
## Instalar Helm

    - curl -L https://git.io/get_helm.sh | bash

## Configurar Conta de Administração do Deploy via Helm (Tiller)

    - kubectl create -f rbac-config.yaml
    - helm init --service-account tiller --history-max 200
    
## Install a release

    - helm list
    - helm install stable/nginx-ingress --name k8s-nginx-ingress --set controller.replicaCount=3 --set rbac.create=true

## listing release elements

    - kubectl get namespaces
    - kubectl get deployments
    - kubectl get services
    - kubectl get pods
    - helm search
    - helm inspect stable/nginx-ingress
    - helm list
    - helm status k8s-nginx-ingress

## Update a release
    
    - helm upgrade k8s-nginx-ingress stable/nginx-ingress --set fullnameOverride="ingress"
    - kubectl get services
    
## Rolling back a release
    
    - helm rollback k8s-nginx-ingress 1 
    - kubectl get services

## Delete a release
    
    - helm delete k8s-nginx-ingress 
    - kubectl get services
    - helm list --deleted
