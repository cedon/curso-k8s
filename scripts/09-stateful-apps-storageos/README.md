# Tuto Stateful APPS - criando volumes (PV e PVC) Com Storage OS

## About

    - Material Curso de Kubernetes
    
## Usage

    - stgos-secret.yaml: arquivo YAML com a senha do Storage Os Operator
    - stgos-install.yaml: arquivo YAML de instalação do Storage OS
    - pvc-stos.yaml: arquivo YAML de criação do pvc para o pod do exemplo
    - debian-pvc.yaml: arquivo YAML de criação do pod de exemplo
    
## Instalar Storage OS Operator

    - kubectl create -f https://github.com/storageos/cluster-operator/releases/download/1.3.0/storageos-operator.yaml
    
## Criar Secret

    - kubectl create -f stgos-secret.yaml
        
## Instalar Storage OS 

    - kubectl create -f stgos-install.yaml
        
## Verificar pods

    -  kubectl -n storageos get pods -w

# Tutorial Guia de Teste Deploy de Volumes com Storge OS

## Definir PVC

    - kubectl create -f pvc-stos.yaml
    - kubectl get pvc

## Criar o Pod que vai montar o pvc anterior

    - kubectl create -f debian-pvc.yaml
    - kubectl get pods

## Executar shell dentro do container e atualize o conteúdo do arquivo

    - $ kubectl exec -it d1 -- bash 
    - root@d1:/# echo "Hello World!" > /mnt/helloworld
    - root@d1:/# cat /mnt/helloworld
    - Hello World!