#!/bin/bash

kubectl create -f secret.yml
kubectl create -f pv-wordpress-mysql.yml
kubectl create -f pvc-wordpress.yml
kubectl create -f pvc-mysql.yml
kubectl create -f mysql-deploy.yml
kubectl create -f wordpress-deploy.yml 