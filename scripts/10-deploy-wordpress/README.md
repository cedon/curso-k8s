# Tuto Wordpress + mysql + kubernetes

## About

    - Material Curso de Kubernetes
    
## Usage

    - secret.yml: arquivo com configurações de senhas para o MySql e Wordpress
    - pv-wordpress-mysql.yml: arquivo com configurações dos volumes persistentes para mysql e wordpress
    - pvc-wordpress.yml: arquivo de requisição de storage do wordpress.
    - pvc-mysql.yml: arquivo de requisição de storage do mysql.
    - mysql-deploy.yml: arquivo de deploy do mysql.
    - wordpress-deploy.yml: arquivo de deploy do wordpress.
    - deploy.sh: arquivo de deploy automatizado do ambiente.


## Tuto

### Configurar NFS
    - apt install nfs-server -y

### sudo vim /etc/exports
    - /     172.31.32.0/24(rw,sync,no_root_squash)
    - 172.31.32.0/24 subrede privada do cluster

### Criar diretórios de backup para os volumes do MySql e Wordpress:
    -sudo mkdir /{mysql,html}
    -sudo chmod -R 755 /{mysql,html}
    -sudo chown nfsnobody:nfsnobody /{mysql,html}
    -sudo systemctl enable rpcbind ; $ sudo systemctl enable nfs-server
    -sudo systemctl enable nfs-lock ; $ sudo systemctl enable nfs-idmap
    -sudo systemctl start rpcbind ; $ sudo systemctl start nfs-server
    -sudo systemctl start nfs-lock ; $ sudo systemctl start nfs-idmap
    
### Deploy do Ambiente
    - kubectl create -f secret.yml
    - kubectl create -f pv-wordpress-mysql.yml
    - kubectl create -f pvc-wordpress.yml
    - kubectl create -f pvc-mysql.yml
    - kubectl create -f mysql-deploy.yml
    - kubectl create -f wordpress-deploy.yml 

## Troubleshooting
    - kubectl get pv,pvc // listar pv,pvc do MySQL & WordPress
    - kubectl get deployment // listar deployment MySQL & WordPress
    - kubectl get pods
    - kubectl describe pods pod-name //pod-name do comando anterior