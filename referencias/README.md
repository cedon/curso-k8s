## REFERENCES

- https://docs.docker.com/develop/develop-images/dockerfile_best-practices/
- https://linuxize.com/post/how-to-remove-docker-images-containers-volumes-and-networks/
- https://afourtech.com/guide-docker-commands-examples/
- https://medium.com/@maniankara/kubernetes-tcp-load-balancer-service-on-premise-non-cloud-f85c9fd8f43c
- https://learnk8s.io/kubernetes-rollbacks/